# Networking 101

## Définir la région et la zone par défaut de l'atelier
```
gcloud config set compute/zone "europe-west4-c"
export ZONE=$(gcloud config get compute/zone)
gcloud config set compute/region "europe-west4"
export REGION=$(gcloud config get compute/region)
```

## Créer le réseau personnalisé
```
gcloud compute networks create taw-custom-network --subnet-mode custom
```

## Créer "subnet-europe-west4" avec un préfixe IP
```
gcloud compute networks subnets create subnet-europe-west4 \
   --network taw-custom-network \
   --region europe-west4 \
   --range 10.0.0.0/16
```

## Créer "subnet-us-central1" avec un préfixe IP
```
gcloud compute networks subnets create subnet-us-central1 \
   --network taw-custom-network \
   --region us-central1 \
   --range 10.1.0.0/16
```

## Créer "subnet-us-west1" avec un préfixe IP
```
gcloud compute networks subnets create subnet-us-west1 \
   --network taw-custom-network \
   --region us-west1 \
   --range 10.2.0.0/16
```

## Afficher la liste de vos réseaux
```
gcloud compute networks subnets list \
   --network taw-custom-network
```

## Créer des règles de pare-feu
```
gcloud compute firewall-rules create nw101-allow-http \
--allow tcp:80 --network taw-custom-network --source-ranges 0.0.0.0/0 \
--target-tags http
```

## ICMP
```
gcloud compute firewall-rules create "nw101-allow-icmp" --allow icmp --network "taw-custom-network" --target-tags rules
```

## Communications internes
```
gcloud compute firewall-rules create "nw101-allow-internal" --allow tcp:0-65535,udp:0-65535,icmp --network "taw-custom-network" --source-ranges "10.0.0.0/16","10.2.0.0/16","10.1.0.0/16"
```

## SSH 
```
gcloud compute firewall-rules create "nw101-allow-ssh" --allow tcp:22 --network "taw-custom-network" --target-tags "ssh"
```

## RDP 
```
gcloud compute firewall-rules create "nw101-allow-rdp" --allow tcp:3389 --network "taw-custom-network"
```

## Créer une instance nommée us-test-01 dans le sous-réseau "subnet-europe-west4"
```
gcloud compute instances create us-test-01 \
--subnet subnet-europe-west4 \
--zone europe-west4-c \
--machine-type e2-standard-2 \
--tags ssh,http,rules
```

## Créer les VM "us-test-02" et "us-test-03" dans les sous-réseaux correspondants
```
gcloud compute instances create us-test-02 \
--subnet subnet-us-central1 \
--zone us-central1-b \
--machine-type e2-standard-2 \
--tags ssh,http,rules

gcloud compute instances create us-test-03 \
--subnet subnet-us-west1 \
--zone us-west1-a \
--machine-type e2-standard-2 \
--tags ssh,http,rules
```

# Principes de base de Terraform
## Créer les VM "us-test-02" et "us-test-03" dans les sous-réseaux correspondants
```
gcloud compute instances create us-test-02 \
--subnet subnet-us-central1 \
--zone us-central1-b \
--machine-type e2-standard-2 \
--tags ssh,http,rules

gcloud compute instances create us-test-03 \
--subnet subnet-us-west1 \
--zone us-west1-a \
--machine-type e2-standard-2 \
--tags ssh,http,rules
```

# Fondamentales sur Terraform


```bash
gcloud auth list
```

La commande ci-dessus, `gcloud auth list`, affiche la liste des comptes Google Cloud authentifiés sur la machine locale. Elle permet de voir quels comptes sont actuellement connectés et actifs pour effectuer des opérations avec gcloud.

```bash
gcloud config list project
```
Résultat de la commande `terraform`
```bash
Usage: terraform [--version] [--help]  [args]

The available commands for execution are listed below. The most common, useful commands are shown first, followed by less common or more advanced commands. If you're just getting started with Terraform, stick with the common commands. For the other commands, please read the help and docs before usage.

Common commands: apply Builds or changes infrastructure console Interactive console for Terraform interpolations destroy Destroy Terraform-managed infrastructure env Workspace management fmt Rewrites config files to canonical format get Download and install modules for the configuration graph Create a visual graph of Terraform resources import Import existing infrastructure into Terraform init Initialize a Terraform working directory output Read an output from a state file plan Generate and show an execution plan providers Prints a tree of the providers used in the configuration push Upload this Terraform module to Atlas to run refresh Update local state file against real resources show Inspect Terraform state or plan taint Manually mark a resource for recreation untaint Manually unmark a resource as tainted validate Validates the Terraform files version Prints the Terraform version workspace Workspace management

All other commands: debug Debug output management (experimental) force-unlock Manually unlock the terraform state state Advanced state management 
```

La commande ci-dessus, `gcloud config list project`, affiche le projet Google Cloud actuellement configuré et utilisé par les commandes gcloud. Cela permet de vérifier rapidement quel projet est défini comme projet par défaut pour les opérations gcloud.

```bash
touch instance.tf
``` 
La commande `touch instance.tf` en bash crée un nouveau fichier vide nommé "instance.tf".

Ensuite aller dans l'editeur de texte de google et ajouter le code ci-dessous dans instance.tf
```bash
resource "google_compute_instance" "terraform" {
  project      = "qwiklabs-gcp-01-aefd70bc4ef6"
  name         = "terraform"
  machine_type = "e2-medium"
  zone         = "us-east1-b"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
    }
  }

  network_interface {
    network = "default"
    access_config {
    }
  }
}
```

# terraform init

La commande `terraform init` initialise un nouveau répertoire de travail Terraform. Elle configure le répertoire pour utiliser les plugins et modules nécessaires à la gestion de l'infrastructure décrite dans les fichiers de configuration Terraform.

Voici le résultat :
```bash
Initializing the backend...

Initializing provider plugins...
- Finding latest version of hashicorp/google...
- Installing hashicorp/google v5.15.0...
- Installed hashicorp/google v5.15.0 (signed by HashiCorp)

Terraform has created a lock file .terraform.lock.hcl to record the provider
selections it made above. Include this file in your version control repository
so that Terraform can guarantee to make the same selections by default when
you run "terraform init" in the future.

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
```

# terraform plan

La commande `terraform plan` est utilisée pour créer un plan d'exécution Terraform. Elle analyse les fichiers de configuration Terraform dans le répertoire de travail et affiche un résumé des actions que Terraform exécutera lors de l'application de ces fichiers pour mettre en œuvre l'infrastructure décrite. Cette commande permet de prévisualiser les modifications potentielles avant de les appliquer réellement.

Voici le résultat :
```bash
Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # google_compute_instance.terraform will be created
  + resource "google_compute_instance" "terraform" {
      + can_ip_forward       = false
      + cpu_platform         = (known after apply)
      + current_status       = (known after apply)
      + deletion_protection  = false
      + effective_labels     = (known after apply)
      + guest_accelerator    = (known after apply)
      + id                   = (known after apply)
      + instance_id          = (known after apply)
      + label_fingerprint    = (known after apply)
      + machine_type         = "e2-medium"
      + metadata_fingerprint = (known after apply)
      + min_cpu_platform     = (known after apply)
      + name                 = "terraform"
      + project              = "qwiklabs-gcp-01-aefd70bc4ef6"
      + self_link            = (known after apply)
      + tags_fingerprint     = (known after apply)
      + terraform_labels     = (known after apply)
      + zone                 = "us-east1-b"

      + boot_disk {
          + auto_delete                = true
          + device_name                = (known after apply)
          + disk_encryption_key_sha256 = (known after apply)
          + kms_key_self_link          = (known after apply)
          + mode                       = "READ_WRITE"
          + source                     = (known after apply)

          + initialize_params {
              + image                  = "debian-cloud/debian-11"
              + labels                 = (known after apply)
              + provisioned_iops       = (known after apply)
              + provisioned_throughput = (known after apply)
              + size                   = (known after apply)
              + type                   = (known after apply)
            }
        }
              + network_interface {
          + internal_ipv6_prefix_length = (known after apply)
          + ipv6_access_type            = (known after apply)
          + ipv6_address                = (known after apply)
          + name                        = (known after apply)
          + network                     = "default"
          + network_ip                  = (known after apply)
          + stack_type                  = (known after apply)
          + subnetwork                  = (known after apply)
          + subnetwork_project          = (known after apply)

          + access_config {
              + nat_ip       = (known after apply)
              + network_tier = (known after apply)
            }
        }
    }

Plan: 1 to add, 0 to change, 0 to destroy.

────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────

```

# terraform apply

La commande `terraform apply` est utilisée pour appliquer les changements décrits dans les fichiers de configuration Terraform. Elle crée, met à jour ou supprime les ressources selon les modifications apportées au code Terraform depuis la dernière exécution. Avant d'appliquer les modifications, Terraform affiche un résumé des actions qu'il va entreprendre et demande une confirmation avant de procéder.

Voici le résultat :
```bash
Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # google_compute_instance.terraform will be created
  + resource "google_compute_instance" "terraform" {
      + can_ip_forward       = false
      + cpu_platform         = (known after apply)
      + current_status       = (known after apply)
      + deletion_protection  = false
      + effective_labels     = (known after apply)
      + guest_accelerator    = (known after apply)
      + id                   = (known after apply)
      + instance_id          = (known after apply)
      + label_fingerprint    = (known after apply)
      + machine_type         = "e2-medium"
      + metadata_fingerprint = (known after apply)
      + min_cpu_platform     = (known after apply)
      + name                 = "terraform"
      + project              = "qwiklabs-gcp-01-aefd70bc4ef6"
      + self_link            = (known after apply)
      + tags_fingerprint     = (known after apply)
      + terraform_labels     = (known after apply)
      + zone                 = "us-east1-b"

      + boot_disk {
          + auto_delete                = true
          + device_name                = (known after apply)
          + disk_encryption_key_sha256 = (known after apply)
          + kms_key_self_link          = (known after apply)
          + mode                       = "READ_WRITE"
          + source                     = (known after apply)

          + initialize_params {
              + image                  = "debian-cloud/debian-11"
              + labels                 = (known after apply)
              + provisioned_iops       = (known after apply)
              + provisioned_throughput = (known after apply)
              + size                   = (known after apply)
              + type                   = (known after apply)
            }
        }
      + network_interface {
          + internal_ipv6_prefix_length = (known after apply)
          + ipv6_access_type            = (known after apply)
          + ipv6_address                = (known after apply)
          + name                        = (known after apply)
          + network                     = "default"
          + network_ip                  = (known after apply)
          + stack_type                  = (known after apply)
          + subnetwork                  = (known after apply)
          + subnetwork_project          = (known after apply)

          + access_config {
              + nat_ip       = (known after apply)
              + network_tier = (known after apply)
            }
        }
    }

Plan: 1 to add, 0 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

google_compute_instance.terraform: Creating...
google_compute_instance.terraform: Still creating... [10s elapsed]
google_compute_instance.terraform: Creation complete after 18s [id=projects/qwiklabs-gcp-01-aefd70bc4ef6/zones/us-east1-b/instances/terraform]

Apply complete! Resources: 1 added, 0 changed, 0 destroyed.

```


# terraform show

La commande `terraform show` affiche l'état actuel de l'infrastructure gérée par Terraform. Elle affiche les ressources créées ainsi que leurs attributs et valeurs actuelles. Cette commande est utile pour vérifier l'état actuel de l'infrastructure après l'application des modifications.

Voici le résultat :
```bash
# google_compute_instance.terraform:
resource "google_compute_instance" "terraform" {
    can_ip_forward       = false
    cpu_platform         = "Intel Broadwell"
    current_status       = "RUNNING"
    deletion_protection  = false
    effective_labels     = {}
    enable_display       = false
    guest_accelerator    = []
    id                   = "projects/qwiklabs-gcp-01-aefd70bc4ef6/zones/us-east1-b/instances/terraform"
    instance_id          = "3631703849039533754"
    label_fingerprint    = "42WmSpB8rSM="
    machine_type         = "e2-medium"
    metadata_fingerprint = "WRRcWNyHo0s="
    name                 = "terraform"
    project              = "qwiklabs-gcp-01-aefd70bc4ef6"
    self_link            = "https://www.googleapis.com/compute/v1/projects/qwiklabs-gcp-01-aefd70bc4ef6/zones/us-east1-b/instances/terraform"
    tags_fingerprint     = "42WmSpB8rSM="
    terraform_labels     = {}
    zone                 = "us-east1-b"

    boot_disk {
        auto_delete = true
        device_name = "persistent-disk-0"
        mode        = "READ_WRITE"
        source      = "https://www.googleapis.com/compute/v1/projects/qwiklabs-gcp-01-aefd70bc4ef6/zones/us-east1-b/disks/terraform"

        initialize_params {
            enable_confidential_compute = false
            image                       = "https://www.googleapis.com/compute/v1/projects/debian-cloud/global/images/debian-11-bullseye-v20240110"
            labels                      = {}
            provisioned_iops            = 0
            provisioned_throughput      = 0
            size                        = 10
            type                        = "pd-standard"
        }
    }

    network_interface {
        internal_ipv6_prefix_length = 0
        name                        = "nic0"
        network                     = "https://www.googleapis.com/compute/v1/projects/qwiklabs-gcp-01-aefd70bc4ef6/global/networks/default"
        network_ip                  = "10.142.0.2"
        queue_count                 = 0
        stack_type                  = "IPV4_ONLY"
        subnetwork                  = "https://www.googleapis.com/compute/v1/projects/qwiklabs-gcp-01-aefd70bc4ef6/regions/us-east1/subnetworks/default"
        subnetwork_project          = "qwiklabs-gcp-01-aefd70bc4ef6"

        access_config {
            nat_ip       = "34.138.108.138"
            network_tier = "PREMIUM"
        }
    }

    scheduling {
        automatic_restart   = true
        min_node_cpus       = 0
        on_host_maintenance = "MIGRATE"
        preemptible         = false
        provisioning_model  = "STANDARD"
    }

    shielded_instance_config {
        enable_integrity_monitoring = true
        enable_secure_boot          = false
        enable_vtpm                 = true
    }
}
```

